#Janela de Login	 

Ao abrir o programa, a primeira tela que será mostrada ao usuário será uma janela de autenticação. Ela solicita o login e a senha de um funcionário cadastrados no sistema para posterior acesso ao programa principal. 
1)	Nome: Adriano Moraes
Login: admoraes
Senha: 123

2)	Nome: João da Silva
Login: josilva
Senha: 123

3)	Nome: Maria Cristina
Login: macristina
Senha: 123

#Janela Principal

Após a autenticação, o usuário terá acesso a uma janela no qual poderá consultar e cadastrar clientes, produtos e pedidos. 
As janelas de consulta possuem métodos para importar seus dados, que são chamados pela janela principal antes de serem exibidas. As janelas 

